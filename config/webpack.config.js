const path = require('path')
const baseConfig = require('./webpack.base.config')

module.exports = Object.assign({}, baseConfig, {
  entry: './pexi.js',
  output: {
    path: path.resolve(__dirname, '..', 'dist'),
    filename: 'pexi.js',
    library: 'pexi'
  },
  context: path.resolve(__dirname, '../src')
})
