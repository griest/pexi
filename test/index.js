import chai from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

import pexi from '../src/pexi'
import 'pixi.js/bin/pixi.js'

import unit from './unit'

chai.use(sinonChai)

unit()
