describe('Charm', function () {
  const Charm = pexi.Charm
  describe('constructor', function () {
    it('should initialize the renderer as PIXI', function () {
      const c = new Charm()
      c.renderer.should.equal('pixi')
    })
    it('should throw an error when passed a falsey value', function () {
      (() => new Charm(null)).should.throw(Error);
      (() => new Charm(false)).should.throw(Error)
      // (() => new Charm(undefined)).should.throw(Error); // comment in this line after mock the global PIXI as undefined
    })
  })
})
